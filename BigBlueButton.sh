#!/bin/bash

#    Copyright (C) 2020 Ruben Rodriguez <ruben@trisquel.info>

#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

# This script has been tested on Trisquel 8

HOSTNAME=bbb.foo.bar

# Basic setup
apt-get install --no-install-recommends language-pack-en certbot wget equivs apt-transport-https software-properties-common python3-certbot-nginx apache2-utils

update-locale LANG=en_US.UTF-8

# We don't want to install ttf-mscorefonts, they are not free
cat << EOF > bbb-fakedepends.control
Section: misc
Priority: optional
Standards-Version: 3.9.2
Package: bbb-fakedepends
Version: 0.1
Maintainer: placeholder@package
Depends: ttf-mscorefonts-installer
Provides: ttf-mscorefonts-installer
Description: Placeholder package to provide dependencies for bigbluebutton
 ttf-mscorefonts-installer is not free
EOF
equivs-build bbb-fakedepends.control
dpkg -i bbb-fakedepends_0.1_all.deb

# Add third party repositories

add-apt-repository ppa:bigbluebutton/support -y

wget https://ubuntu.bigbluebutton.org/repo/bigbluebutton.asc -O- | sudo apt-key add -
echo "deb https://ubuntu.bigbluebutton.org/xenial-220/ bigbluebutton-xenial main" | sudo tee /etc/apt/sources.list.d/bigbluebutton.list

add-apt-repository ppa:rmescandon/yq -y

wget -qO- https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
echo 'deb https://deb.nodesource.com/node_8.x xenial main' > /etc/apt/sources.list.d/nodesource.list

wget -qO - https://www.mongodb.org/static/pgp/server-3.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list

# Update lists and install
apt update
apt install nodejs
npm install -g node-gyp

apt-get install bigbluebutton bbb-html5 bbb-demo mongodb-org curl 
rm /etc/nginx/sites-enabled/default

sed 's|.*node main.js|PORT=3000 /usr/bin/node main.js|' -i /usr/share/meteor/bundle/systemd_start.sh
[ "$(lsb_release -sc)" == "flidas" ] && sed 's/xenial/flidas/' -i /usr/bin/bbb-conf

###### Configure SSL

certbot --webroot -w /var/www/html/ -m sysadmin@$HOSTNAME certonly --agree-tos -d $HOSTNAME -n


cat << EOF |sed '/server_name.*$/ r /dev/stdin' -i /etc/nginx/sites-available/bigbluebutton

     listen 443 ssl;
     listen [::]:443 ssl;
     ssl_certificate /etc/letsencrypt/live/$HOSTNAME/fullchain.pem;
     ssl_certificate_key /etc/letsencrypt/live/$HOSTNAME/privkey.pem;
     ssl_session_cache shared:SSL:10m;
     ssl_session_timeout 10m;
     ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
     ssl_ciphers "ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS:!AES256";
     ssl_prefer_server_ciphers on;
     #ssl_dhparam /etc/nginx/ssl/dhp-4096.pem;
EOF


sed '/proxy_pass/s/http/https/; /proxy_pass/s/5066/7443/' -i /etc/bigbluebutton/nginx/sip.nginx
sed '/bigbluebutton.web.serverURL/s/http/https/' -i /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
sed 's/http/https/' -i /usr/share/red5/webapps/screenshare/WEB-INF/screenshare.properties
sed 's|http://|https://|g' -i /var/www/bigbluebutton/client/conf/config.xml
sed 's/http:/https:/;s/ws:/wss:/' -i /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml
sed '/playback_protocol/s/http/https/' -i /usr/local/bigbluebutton/core/scripts/bigbluebutton.yml
sed 's/http/https/' -i /var/lib/tomcat7/webapps/demo/bbb_api_conf.jsp

# Default to HTML5, disables Flash support
sed 's/JoinViaHTML5Client=false/JoinViaHTML5Client=true/' -i /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties

# Fix integration issue with Canvas
sed 's/\d{5,6}/\d{5,6}|\\d{20}/' -i /opt/freeswitch/conf/dialplan/default/bbb_echo_test.xml /opt/freeswitch/conf/dialplan/default/bbb_conference.xml

bbb-conf --setip $HOSTNAME
service nginx restart

apt-get install ufw
ufw allow ssh
ufw allow 80/tcp
ufw allow 443/tcp
ufw allow 1935/tcp
ufw allow 7443/tcp
ufw allow 16384:32768/udp
ufw default deny
ufw enable

# Configure passwords

htpasswd -c /etc/nginx/htpasswd username

cat << EOF |sed '/fastcgi_params;$/ r /dev/stdin' -i /etc/bigbluebutton/nginx/demo.nginx

        auth_basic "Private server. Contact sysadmin@$HOSTNAME if you need a BigBlueButton instance for your classroom";
        auth_basic_user_file /etc/nginx/htpasswd;

EOF

service nginx restart
