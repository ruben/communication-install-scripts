#!/bin/bash
  
#    Copyright (C) 2020 Ruben Rodriguez <ruben@trisquel.info>

#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

# This script has been tested on Trisquel 9

# References https://github.com/instructure/canvas-lms/wiki/Production-Start

HOSTNAME=canvas.foo.bar

# Install basic packages for this script
apt-get install wget software-properties-common git pwgen

# Add repositories to apt
wget -qO- https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add -
echo 'deb https://deb.nodesource.com/node_10.x bionic main' > /etc/apt/sources.list.d/nodesource.list

add-apt-repository ppa:chris-lea/redis-server -y

add-apt-repository ppa:brightbox/ruby-ng -y

wget -qO- https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger bionic main > /etc/apt/sources.list.d/passenger.list'

# Update lists and install dependencies

apt-get update
apt-get dist-upgrade
apt-get install nodejs ruby2.4 ruby2.4-dev zlib1g-dev libxml2-dev \
                libsqlite3-dev postgresql libpq-dev libxmlsec1-dev \
                curl make g++ yarn=1.10.1-1 postgresql-10 passenger \
                libapache2-mod-passenger apache2 python3-certbot-apache \
                language-pack-en redis-server

update-locale LANG=en_US.UTF-8

# Configure database

DBPASS=$(pwgen 20 -n1)
su postgres -l -c "psql -c \"CREATE USER canvas WITH PASSWORD '$DBPASS';\""
su postgres -l -c 'createdb canvas_development --owner=canvas'
su postgres -l -c 'createdb canvas_production --owner=canvas'
su postgres -l -c 'createdb canvas_test --owner=canvas'

# Install canvas from git

adduser canvas --system --home /srv/canvas --shell /bin/bash
cd /srv/canvas

su canvas -l -c 'git clone https://github.com/instructure/canvas-lms.git /srv/canvas'
su canvas -l -c 'git -C /srv/canvas checkout stable'

for config in amazon_s3 dynamic_settings database logging\
   delayed_jobs domain file_store outgoing_mail security external_migration; \
   do cp /srv/canvas/config/$config.yml.example /srv/canvas/config/$config.yml; done 

su canvas -c "sed 's/your_password/$DBPASS/' -i /srv/canvas/config/database.yml"
su canvas -c "sed 's/12345/$(pwgen 50 -n1)/' -i /srv/canvas/config/security.yml"
su canvas -c "sed 's/canvas.example.com/$HOSTNAME/' -i /srv/canvas/config/domain.yml"
su canvas -c "sed '/production:/a\ \ log_level: info' -i /srv/canvas/config/logging.yml"

gem install bundler -v 1.13.6
npm install -g coffee-script@1.6.2

su canvas -l -c 'RAILS_ENV=production bundle install --path vendor/bundle'
su canvas -l -c 'yarn install --pure-lockfile && yarn install --pure-lockfile'
su canvas -l -c 'RAILS_ENV=production bundle exec rails canvas:compile_assets'
su canvas -l -c 'RAILS_ENV=production bundle exec rails db:initial_setup'

# You can run this for testing
# su -l canvas -c 'bundle exec rails server'

# Configure certs and apache

certbot --apache -m sysadmin@$HOSTNAME certonly --agree-tos -d $HOSTNAME  -n

a2enmod rewrite
a2enmod passenger
a2enmod ssl

unlink /etc/apache2/sites-enabled/000-default.conf

cat << EOF > /srv/canvas/config/cache_store.yml
test:
    cache_store: redis_store
development:
    cache_store: redis_store
production:
    cache_store: redis_store
EOF

cat << EOF > /srv/canvas/config/redis.yml
production:
  servers:
    - redis://localhost
EOF

systemctl enable redis-server
systemctl start redis-server

cat << EOF > /etc/apache2/sites-enabled/canvas.conf
  #PassengerLogLevel 5
  PassengerStartTimeout 300
  PassengerDefaultUser canvas
<VirtualHost *:80>
  ServerName $HOSTNAME
  ServerAdmin sysadmin@$HOSTNAME
  DocumentRoot /srv/canvas/public
  RewriteEngine On
  RewriteCond %{HTTP:X-Forwarded-Proto} !=https
  RewriteCond %{REQUEST_URI} !^/health_check
  RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [L]
  ErrorLog /var/log/apache2/canvas_errors.log
  LogLevel warn
  CustomLog /var/log/apache2/canvas_access.log combined
  SetEnv RAILS_ENV production
  <Directory /srv/canvas/public>
    Allow from all
    Options -MultiViews
  </Directory>
</VirtualHost>
<VirtualHost *:443>
  ServerName $HOSTNAME
  ServerAdmin sysadmin@$HOSTNAME
  DocumentRoot /srv/canvas/public
  ErrorLog /var/log/apache2/canvas_errors.log
  LogLevel warn
  CustomLog /var/log/apache2/canvas_ssl_access.log combined
  SSLEngine on
  BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
  SSLCertificateFile /etc/letsencrypt/live/$HOSTNAME/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/$HOSTNAME/privkey.pem
  SetEnv RAILS_ENV production
  <Directory /srv/canvas/public>
   Options All
    AllowOverride All
    Require all granted
  </Directory>
</VirtualHost>
EOF

# Configure your email output by editing /srv/canvas/config/outgoing_mail.yml

service apache2 restart

# Enable background job processing

ln -s /srv/canvas/script/canvas_init /etc/init.d/canvas_init
update-rc.d canvas_init defaults
/etc/init.d/canvas_init start
